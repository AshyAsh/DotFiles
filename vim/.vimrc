set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'preservim/nerdtree'
Plugin 'justmao945/vim-clang'
"Plugin 'sheerun/vim-polyglot'
"Plugin 'vim-airline/vim-airline'
"Plugin 'mattn/emmet-vim'

call vundle#end()            " required
filetype plugin indent on    " required

" Relative numbers
set number
set relativenumber

" Set the TAB's size to 2 characters
set tabstop=2
set shiftwidth=2
set expandtab

" Set the maximum of 80 characters per line
set colorcolumn=80
set textwidth=80
set formatoptions+=t

" vim-clang options
let g:clang_debug=2
let g:clang_format_auto=1
let g:clang_check_syntax_auto=1
let g:clang_verbose_pmenu=1
let g:clang_format_style="GNU"
