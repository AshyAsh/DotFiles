export VISUAL="emacs -nw"
export EDITOR="emacs -nw"

export PATH="${PATH}:/home/wva/.gem/ruby/2.7.0/bin/"

alias lynxp="proxychains4 lynx"
alias lynxd="lynxp duckduckgo.com"

alias emacs="emacs -nw"

alias ga="git add ."
alias gc="git commit -m"
alias gao="git remote add origin"
alias gp="git push --set-upstream origin master"
alias gdiff="git diff"

alias ins="sudo pacman -S"
alias upd="sudo pacman -Sy"
alias upg="upd && pacman -Syyu"
alias rem="sudo pacman -Rs"
alias searx="sudo pacman -Ss"

alias dog="cat"

alias tor="/home/$(whoami)/Software/tor/Browser/start-tor-browser --detach"
alias blender="/home/$(whoami)/Software/blender/blender"
alias godot="/home/$(whoami)/Software/Godot/Godot.64"
